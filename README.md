# MRCL Discord Translations
This repository contains the translations for certain "articles" on the [MRCL Discord](https://discord.mrcl.cc).

Contributions for localisation to your language are welcome, please make sure to translate from **English** as accurately as you can. Take a look at [this guide](https://gitlab.com/mrcl1/wiki/-/blob/master/Contributing.md) for instructions on how to contribute (just make sure to switch out the Wiki Gitlab links for the Discord Translations)
