MRCL to serwer Minecraft stworzony dla wersji Java Edition gry, dobrą wiadomością jest to, że nie potrzebujesz żadnych modów by dołączyć do gry! Wystarczy tylko jedna paczka zasobów, która pobiera się automatycznie, gdy tylko wejdziesz na serwer.

Adres IP jak i Wersja mogą się czasami zmienić, ale możesz zawsze użyć komend `/ip` oraz `/version` na kanale <#949021380500947014>. Serwer ma uruchomiony tryb "offline", który pozwala na dołączanie graczy bez zakupionej gry (Nie musisz kupować Minecrafta, aby grać)

Proszę nie pytać na serwerze jak możesz dostać taką wersję gry. Mimo tego, że pozwalamy takim graczom na granie na serwerze to nie "popieramy" tego.
