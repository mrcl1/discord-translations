Se tiveres um problema, por favor lê primeiro o <#501395380119011358> FAQ (Perguntas Mais Frequentes) para ver se o teu problema está na lista. Se o teu problema não estiver na lista, ou a solução não funcionar, cria um ticket usando o embed em <#501395380119011358>, preenche as instruções, e lá estaremos para ajudar.

