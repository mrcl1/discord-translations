Tenta mudar as tua definições de Pacote de Recursos na o Editar Informação do Servidor no menu Multijogador para ‘Activo’ e reinicia o jogo.

Se não funcionar, vai para a tua pasta `%appdata%/roaming/.minecraft` (Pode ser diferente dependendo do jogador. Se não estiver lá, não vamos poter ajudar muito, já que é a pasta padrão). Encontra a pasta server-resourcepacks e apaga todo o interior. Reinicia o jogo e tenta novamente.

Se já tentaste tudo isto e ainda não ter funcionado, abre um ticket com o embed abaixo.

