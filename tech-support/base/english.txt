changed-username: I changed my username and now I get this message when joining! `You should join using username OLD NAME, not NEW NAME.`
forgot-password: I forgot my password to login to the server!
i-cant-connect: I can't connect to the server!/Connecting to the server gives me an error!
invalid-session: I get “Invalid Session” when trying to connect to the server
keeping-advancements: How do I keep my advancements after using `/premium`?
the-rp-wont-work: How do I get the RP/The RP won't work
