Il nostro sistema di autenticazione è un pochino suscettibile riguardo gli utenti che cercano di accedere con una lettera di forma diversa nei loro username, maiuscola anziché minuscola o viceversa, rispetto all'username registrato originariamente, ciò significa che avremo bisogno di rimuovere il tuo account dai nostri sistemi.
Se dovessi incontrare questo errore, prova a cambiare il tuo username in quello specificato dalla sezione OLD NAME del messaggio.
Se non dovessi riuscirci, crea un ticket utilizzando l'integrazione di seguito.
